
public class Item implements ItemInterface{

	private String sName; 
	private String sDescription;
	private int iPrice; 
	private int iAmount; 
	private boolean bUsable = true; 
	private String sAllAttributes = "";
	  
	public Item(String sName, String sDescription, int iPrice, int iAmount) {
	
		this.sName = sName; 
		this.sDescription = sDescription;
		this.iPrice = iPrice; 
		this.iAmount = iAmount; 
		this.bUsable = true; 
	}
	
	public Item() {
		this.sName = "Default item";
		this.sDescription = "Does something"; 
		this.iPrice = 0; 
		this.iAmount = 1; 
		this.bUsable = true; 
	}
	
	public Item(String sName, String sDescription) {
		this.sName = sName; 
		this.sDescription = sDescription; 
		this.iPrice = 0; 
		this.iAmount = 1; 
		this.bUsable = true; 
	}
	
	public void use() {
	} 
	
	public String toString() {
		
	 sAllAttributes += "" +sName + "\n";
	 sAllAttributes += sDescription + "\n";
	 sAllAttributes += "Price: " + iPrice + " �" + "\n";
	 sAllAttributes += "Amount: " + iAmount + "\n";
	 return sAllAttributes;
		
	}
  
	public String getName() {
		return sName; 
	}
	
	public void setName(String sName) {
		this.sName = sName; 
	}
	
	public String getDescription() {
		return sDescription;
	}
	
	public int getAmount() {
		return iAmount;
	}
	
	public void setAmount(int iAmount) {
		this.iAmount = iAmount; 
	}
  
	public void setDescription(String sDescription) {
		this.sDescription = sDescription; 
	}
	
	public int getPrice() {
		return iPrice;
	}
  
	public void setPrice(int iPrice) {
		this.iPrice = iPrice; 
	}
	public boolean getUsable() {
		return bUsable;
	}
	public void setUsable(boolean bUsable) {
		this.bUsable = bUsable; 
	}
	
  }


