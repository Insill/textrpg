
/**
 * @(#)Command.java
 * A class responsible for various actions such as showing the player's inventory, equipping and unequipping apparel and calling the method/class for exploring.
 * TODO: Integrating this menu system into commands, now one can just walk from one room to another
 * TODO: Implementing room exploring. 
 * @author Insill
 */
import java.util.Scanner;

/**
 * @(#)Command.java
 * A class responsible for various actions such as showing the player's inventory, equipping and unequipping apparel and calling the method/class for exploring.
 * TODO: Integrating this menu system into commands, now one can just walk from one room to another
 * TODO: Implementing room exploring. 
 * @author Insill
 */
import java.util.Scanner;

public class Command{

	public Scanner scanner = new Scanner(System.in); 
	public GameCharacter player; 
	public static final int INVENTORY = 1; 
	public static final int SEARCH = 2; 
	public static final int STATS = 3; 
	public static final String[] commandName = {
			"UNDEFINED",
			"INVENTORY",
			"STATS", 
			"SEARCH",
	};
	// 
	public void showInventory()  {
		int iCommand = -1; 
		if(!player.getInventory().isEmpty()) {
			player.getInventory().printAll();
			while(iCommand < 0) {
				System.out.println("Choose 1- Items 2- Weapons 3- Apparel 4- Stats 0- Return");
				iCommand = scanner.nextInt(); 
				switch(iCommand) {
					case 0: quit(); break;
					case 2: showItems(); break;
					case 3: showWeapons(); break;
					case 4: showApparel(); break; 
					case 5: showStats(); break; 
					default: showInventory();
				}
			}
		}
			
	}
	
	private void showStats() {
		player.toString();
	}

	public void showItems() {
		int iCommand = -1; 
		player.getInventory().printItems();
		while(iCommand < -2) {
			System.out.println("Choose item by number, quit by typing -2: "); 
			iCommand = scanner.nextInt(); 
		}
		if(iCommand == -2 ) {
			showInventory(); 
		} else {
			Item item = player.getInventory().getItems().get(iCommand); 
			iCommand = 0; 
			System.out.println(item.getDescription()+"\n---------n");
			if(item.getUsable() == true) {
				while(iCommand == 0) {
					System.out.println("Use this item? 1 - Yes 2 - No:\t");
					iCommand = scanner.nextInt();
				}
				switch(iCommand) {
					case 1: player.getInventory().useItem(item);
					case 2: showItems();
				}
			} else {
				showItems(); 
			}
		}
	}
	public void showWeapons() {
		int iCommand = -1; 
		player.getInventory().printWeapons();
		while(iCommand < -2) {
			System.out.println("Choose weapon by number, quit by typing -2: "); 
			iCommand = scanner.nextInt(); 
		}
		if(iCommand == -2 ) {
			quit(); 
		} else {
			Weapon weapon = player.getInventory().getWeapons().get(iCommand); 
			iCommand = 0; 
			System.out.println(weapon.getDescription()+"\n---------n");
			if(weapon.getEquipped() == false) {
				while(iCommand == 0) {
					System.out.println("Equip this item? 1 - Yes 2 - No:\t");
					iCommand = scanner.nextInt();
				}
				switch(iCommand) {
					case 1: equipWeapon(weapon);
					case 2: showItems();
				}
			}
			else if(weapon.getEquipped() == true) {
				while(iCommand == 0) {
					System.out.println("Enequip this item? 1 - Yes 2 - No:\t");
					iCommand = scanner.nextInt(); 
				}
				switch(iCommand) {
				case 1: unEquipWeapon(weapon); 
				case 2: showItems(); 
				}
			}
			}
		}
	public void showApparel() {
		int iCommand = -1; 
		player.getInventory().printApparels();
		while(iCommand < -2) {
			System.out.println("Choose apparel by number, quit by typing -2: "); 
			iCommand = scanner.nextInt(); 
		}
		if(iCommand == -2 ) {
			quit(); 
		} else {
			Apparel apparel = player.getInventory().getApparels().get(iCommand); 
			iCommand = 0; 
			System.out.println(apparel.getDescription()+"\n---------n");
			if(apparel.getEquipped() == false) {
				while(iCommand == 0) {
					System.out.println("Equip this item? 1 - Yes 2 - No:\t");
					iCommand = scanner.nextInt();
				}
				switch(iCommand) {
					case 1: equipApparel(apparel);
					case 2: showItems();
				}
			}
			else if(apparel.getEquipped() == true) {
				while(iCommand == 0) {
					System.out.println("Enequip this item? 1 - Yes 2 - No:\t");
					iCommand = scanner.nextInt(); 
				}
				switch(iCommand) {
				case 1: unEquipApparel(apparel); 
				case 2: showItems(); 
				}
			}
			}
	}
	
	public void search() {	
		System.out.println("Searching the current room"); 
		// Check if room has loot 
		// Check the player's Perception and Luck, it can have an effect on finding loot
		// Creating a Factory instance to spawn a random item, there has to be randomness determining if the player finds any loot and what kind of items will appear
	}
	public void equipWeapon(Weapon weapon) {
		player.setEquippedWeapon(weapon);
		System.out.println(player.getName() + " equipped " + weapon.getName());
		showWeapons(); 
	}
	public void unEquipWeapon(Weapon weapon) {
		player.unEquipWeapon(weapon);
		System.out.println(player.getName() + " unequipped " + weapon.getName());
		showWeapons(); 
	}
	
	public void equipApparel(Apparel apparel) {
		player.setEquippedApparel(apparel);
		System.out.println(player.getName() + " equipped " + apparel.getName());
		showApparel();
	}
	
	public void unEquipApparel(Apparel apparel) {
		player.unEquipApparel(apparel); 
		System.out.println(player.getName() + "unequipped " + apparel.getName());
		showApparel();
	}
	
	public void quit() {
		
	}
}