import java.util.ArrayList;
import java.util.Enumeration;

/*
* Room - represents a gaming Room
*
* Original code : November 13, 1997
* Modified by Insill in 2016
* @author Insill
*/

@SuppressWarnings("serial")
public class Room implements java.io.Serializable
{
	// Member variables
	private String roomTitle;
	private String roomDescription;
	private ArrayList<Exit> Exits;
	private int battleLikehoodRatio; 
	// Blank constructor
	public Room() {
		// Blank title + description
		this.roomTitle = "Blank Room"; 
		this.roomDescription = "Blank description"; 
		this.battleLikehoodRatio = 4; 
		Exits = new ArrayList<Exit>();
		
	}

	// Partial constructor
	public Room( String title )	{
		// Assign title
		this.roomTitle = title;
		this.battleLikehoodRatio = 4; 
		// Blank description
		roomDescription = "Blank description";

		// Blank exits
		Exits = new ArrayList<Exit>();
	}

	// Full constructor
	public Room( String title, String description )	{
		// Assign title + description
		this.roomTitle = title;
		this.roomDescription = description;

		// Blank exits
		Exits = new ArrayList<Exit>();
	}

	// toString method
	public String toString()	{
		return roomTitle;
	}

	// Adds an exit to this Room
	public void addExit ( Exit exit ) {
		if(!Exits.contains(exit)) {
			Exits.add (exit);
		}	
	}

	// Removes an exit from this Room
	public void removeExit ( Exit exit ){
		if (Exits.contains (exit))	{
			Exits.remove (exit);
		}
	}

	// Returns a ArrayList of exits
	public ArrayList<Exit> getExits ()	{
		// Return a clone, as we don't want an external
		// object to modify our original ArrayList
		return (ArrayList<Exit>) Exits.clone();
	}
	
	// Returns Room title
	public String getTitle()	{
		return roomTitle;
	}

	// Assigns Room title
	public void setTitle( String roomTitle ){
		this.roomTitle = roomTitle;
	}
	// Returns Room description
	public String getDescription(){
		return roomDescription;
	}

	// Assigns Room description
	public void setDescription( String roomDescription ){
		this.roomDescription = roomDescription;
	}
	public void setBattleLikehoodRatio(int battleLikehoodRatio) {
		this.battleLikehoodRatio = battleLikehoodRatio; 
	}
	public int getBattleLikehoodRatio() {
		return battleLikehoodRatio;
	}
}

