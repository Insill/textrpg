import java.util.*; 
/**
 * A class representing the player's character. Contains all the attributes (Strength, Perception, Endurance, Charisma, Intelligence, Agility, Luck) that govern things the player does. 
 * The Command class calls methods of this class when actions related to equipping or unequipping weapons and apparel are needed.
 * TODO: Functionality for leveling up
 * @author Insill
 */

public class GameCharacter extends Character {

	private int iExperience; 
	private int iStrength;
	private int iPerception; 
	private int iEndurance;
	private int iCharisma;
	private int iIntelligence;
	private int iAgility;
	private int iLuck;
	

	private int iMaxHitPoints;
	private int iCurrentHitPoints; 
	public String sAllAttributes = ""; 

	private int iHitPointBoost;
	private int iStrengthBoost;
	private int iPerceptionBoost;
	private int iEnduranceBoost;
	private int iIntelligenceBoost;
	private int iCharismaBoost;
	private int iAgilityBoost;
	private int iLuckBoost;
	
	public GameCharacter() {
		
		super();
		this.iExperience = 0; 
		this.iMaxHitPoints = 50;
		this.iCurrentHitPoints = iMaxHitPoints;
		this.iStrength = 5;
		this.iPerception = 5;
		this.iEndurance = 5;
		this.iIntelligence = 5;
		this.iCharisma = 5; 
		this.iAgility = 5;
		this.iLuck = 5; 
	}

	public GameCharacter(String sName, String sState,String sDescription) {
		super(sName, sState, sDescription);
		this.iExperience = 0; 
		this.iStrength = 5;
		this.iPerception = 5;
		this.iEndurance = 5;
		this.iIntelligence = 5;
		this.iCharisma = 5; 
		this.iAgility = 5;
		this.iLuck = 5; 
	}
	

	public GameCharacter(String sName, String sState, String sDescription, int iLevel, int iMaxHitPoints, int iExperience, 
						int iStrength, int iPerception, int iEndurance, 
						int iCharisma, int iIntelligence, int iAgility, int iLuck) {
							
		super(sName, sState, sDescription);
		this.iExperience = iExperience; 
		this.iStrength = iStrength; 
		this.iPerception = iPerception; 
		this.iEndurance = iEndurance;
		this.iIntelligence = iIntelligence; 
		this.iCharisma = iCharisma; 
		this.iAgility = iAgility;
		this.iLuck = iLuck; 
	}

	public String toString() {
		
		sAllAttributes += super.toString();
		sAllAttributes += "EXP Points: " + iExperience + "\n";
		sAllAttributes += "HP : " + iCurrentHitPoints + "/"+iMaxHitPoints +"\n";
		sAllAttributes += "Strength: " + iStrength + "\n";
		sAllAttributes += "Perception: " + iPerception + "\n";
		sAllAttributes += "Endurance: " + iEndurance + "\n";
		sAllAttributes += "Charisma: " + iCharisma + "\n";
		sAllAttributes += "Intelligence: " + iIntelligence  + "\n";
		sAllAttributes += "Agility: " + iAgility + "\n";
		sAllAttributes += "Luck: " + iLuck + "\n---------------";	
		return sAllAttributes; 
		
	}
	
	public int iExpToLevel(int iLevel) {
		return 128 * iLevel * iLevel;
	}
	
	public void iLevelUP() {
		
		if(this.iExperience >= iExpToLevel(this.getLevel()+1)) {
			
			this.setLevel(this.getLevel()+1); 
			
		    iHitPointBoost = 0;
		    iStrengthBoost = 0;
			iPerceptionBoost = 0;
		    iEnduranceBoost = 0;
			iIntelligenceBoost = 0;
			iCharismaBoost = 0;
			iAgilityBoost = 0;
			iLuckBoost = 0;
	
			if(this.getLevel() % 3 == 0) {
			
				iHitPointBoost = 10 + (int)(Math.floor(Math.random() * 10) % 4) + this.iEndurance / 4;
			}
			else {
			
				iHitPointBoost = this.iEndurance / 4; 
			}
			iStrengthBoost = 1;
			iPerceptionBoost = 1; 
			iEnduranceBoost = 1;
			iIntelligenceBoost = 1; 
			iCharismaBoost = 1; 
			iAgilityBoost = 1;
			iLuckBoost = 1; 

			this.setMaxHitPoints(this.getMaxHitPoints() + iHitPointBoost); 
			this.iMaxHitPoints += iHitPointBoost; 
			this.iStrength += iStrengthBoost; 
			this.iEndurance += iEnduranceBoost; 
			this.iIntelligence += iIntelligenceBoost; 
			this.iCharisma+= iCharismaBoost; 
			this.iAgility += iAgilityBoost; 
			this.iLuck += iLuckBoost; 
		}
	}
	
	public int getMaxHitPoints(){
		return iMaxHitPoints;
	}
	public void setMaxHitPoints(int iMaxHitPoints){
		this.iMaxHitPoints = iMaxHitPoints; 
	}
	public int getCurrentHitPoints() {
		return iCurrentHitPoints; 
	}
	public void setCurrentHitPoints(int iCurrentHitPoints) {
		this.iCurrentHitPoints = iCurrentHitPoints; 
	}
	public int getStrength() {
		return iStrength; 
	}
	public void setStrength(int iStrength){
		this.iStrength = iStrength;
	}
	public int getPerception() {
		return iPerception;
	}
	public void setPerception(int iPerception) {
		this.iPerception = iPerception;
	}
	public int getEndurance() {
		return iEndurance;
	}
	public void setEndurance(int iEndurance) {
		this.iEndurance = iEndurance; 
	}
		 
	public int getCharisma() {
		return iCharisma;
	}

	public void setCharisma(int iCharisma) {
		this.iCharisma = iCharisma; 
	}
	public int getIntelligence() {
		return iIntelligence;
	}
	public void setIntelligence(int iIntelligence) {
		this.iIntelligence = iIntelligence; 
	}
	public int getAgility() {
		return iAgility;
	}
	public void setAgility(int iAgility){
		this.iAgility = iAgility; 
	}
	public int getLuck() {
		return iLuck; 
	}
	public void setLuck (int iLuck) {
		this.iLuck = iLuck; 
	}

	public void unEquipApparel(Apparel apparel) {
		Inventory inventory = getInventory();
		if(inventory.Apparels.contains(apparel) && apparel.getEquipped() == true) 
			apparel.setEquipped(false);
	}
	
  
	public void unEquipWeapon(Weapon weapon) {
		Inventory inventory = getInventory();
		if(inventory.Weapons.contains(weapon) && weapon.getEquipped() == true)
			weapon.setEquipped(false);
	}


}