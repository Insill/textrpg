/**
* @(#)Character.java
* A class for the game's characters that is inherited by the Player and Enemy classes. Every character has a name, state and description so it's very basic. 
* Player and Enemy classes call the Character constructor in their own constructors.
* @author Insill
*/
public class Character {

	private String sName;
	private String sState; 
	private String sDescription; 
	private String sAllAttributes = ""; 
	private int iMaxHitPoints; 
	private int iCurrentHitPoints; 
	private int iLevel; 
	private Weapon equippedWeapon; 
	private Apparel equippedApparel; 
	private Inventory inventory; 
	
	public Character(String sName, String sState,
							String sDescription) {

		this.sName = sName; 
		this.sState = sState; 
		this.sDescription = sDescription; 
		this.iLevel = 1; 
		this.iCurrentHitPoints = iMaxHitPoints; 
		this.iMaxHitPoints = 10; 
		this.inventory = new Inventory(); 
		this.equippedApparel = null; 
		this.equippedWeapon = null; 
	}
	public Character() {
		this.sName = "Example character";
		this.sState = "Normal";
		this.sDescription = " ";
		this.iLevel = 1; 
		this.iCurrentHitPoints = iMaxHitPoints; 
		this.iMaxHitPoints = 10; 
		this.inventory = new Inventory(); 
		this.equippedApparel = null; 
		this.equippedWeapon = null; 
	}

	public String toString() {
		
		sAllAttributes += "---------------\n";
		sAllAttributes += " " + sName + "\n";
		sAllAttributes += " " + sState + "\n";
		sAllAttributes += " " + sDescription + "\n";
		sAllAttributes += " Level : " + iLevel + "\n";  
		sAllAttributes += " HP:" + iCurrentHitPoints + "/" + iMaxHitPoints + "\n"; 
		sAllAttributes += "---------------\n";
		return sAllAttributes; 
		
	}
	public void setName(String sName) {
		this.sName = sName; 
	}
	public String getName() {
		return this.sName;
	}

	public void setState(String sState){
		this.sState = sState; 
	}

	public String getState() {
		return this.sState;
	}
		
	public String getDescription() {
		return this.sDescription; 
	}
	public boolean isAlive() {
		return iCurrentHitPoints > 0;
		
	}
	public void setDescription (String sDescription) {
		this.sDescription = sDescription; 	
	}
    
	public int getLevel() {
		return this.iLevel; 
	}
	public void setLevel(int iLevel) {
		this.iLevel = iLevel; 
	}
	public int getCurrentHitPoints() {
		return this.iCurrentHitPoints;	
	}
	public void setCurrentHitPoints(int iCurrentHitPoints ) {
		this.iCurrentHitPoints = iCurrentHitPoints; 
	}
	public int getMaxHitPoints() {
		return this.iMaxHitPoints; 
	}
	public void setMaxHitPoints(int iMaxHitPoints) {
		this.iMaxHitPoints = iMaxHitPoints; 
	}
	public Inventory getInventory() {
		return this.inventory; 
	}
	public void setEquippedApparel(Apparel apparel) {
		if(inventory.Apparels.contains(apparel) && apparel.getEquipped() == false) 
			apparel.setEquipped(true);
			this.equippedApparel = apparel; 
	}
	public void setEquippedWeapon(Weapon weapon) {
		
		if(inventory.Weapons.contains(weapon) && weapon.getEquipped() == false)
			weapon.setEquipped(true);
			this.equippedWeapon = weapon; 
	}
	public Weapon getEquippedWeapon() {
		Weapon equippedWeapon = null;
		for(Weapon weapon: inventory.Weapons) {
			if(weapon.getEquipped() == true) {
				equippedWeapon = weapon;
			}
		}
		return equippedWeapon;
	}
	public Apparel getEquippedApparel() {
		Apparel equippedApparel = null; 
		for(Apparel apparel : inventory.Apparels) {
			if(apparel.getEquipped() == true) {
				equippedApparel = apparel; 
			}
		}
		return equippedApparel; 
	}
}