
/**
 * @(#)Apparel.java
 * A class for clothes and armor. When equipped, some clothes can change the player's attributes. Armors may get their own class to suit the battle mechanics.
 * 
 * @author Insill
 */
public class Apparel extends Item{

	private int iStrengthBoost;
	private int iPerceptionBoost;
	private int iEnduranceBoost;
	private int iCharismaBoost;
	private int iIntelligenceBoost;
	private int iLuckBoost;
	private int iAgilityBoost;
	private boolean bEquipped;

	public Apparel(String sName, String sDescription,boolean bUsable, int iPrice, int iAmount, int iStrengthBoost, int iPerceptionBoost, int iEnduranceBoost, int iCharismaBoost, int iIntelligenceBoost, int iAgilityBoost, int iLuckBoost) {
		super(sName, sDescription, iPrice, iAmount);
		this.setEquipped(false); 
		this.setStrengthBoost(iStrengthBoost); 
		this.setPerceptionBoost(iPerceptionBoost); 
		this.setEnduranceBoost(iEnduranceBoost); 
		this.setCharismaBoost(iCharismaBoost); 
		this.setIntelligenceBoost(iIntelligenceBoost); 
		this.setAgilityBoost(iAgilityBoost); 
		this.setLuckBoost(iLuckBoost); 
	}

	public Apparel(String sName, String sDescription, int iPrice, int iAmount) {
		super(sName,sDescription,iPrice,iAmount); 
	}

	public Apparel(String sName, String sDescription) {
		super(sName, sDescription);
		// TODO Auto-generated constructor stub
	}
	public Apparel() {
		super();
	}
	public int getStrengthBoost() {
		return this.iStrengthBoost;
	}

	public void setStrengthBoost(int iStrengthBoost) {
		this.iStrengthBoost = iStrengthBoost;
	}

	public int getPerceptionBoost() {
		return this.iPerceptionBoost;
	}

	public void setPerceptionBoost(int iPerceptionBoost) {
		this.iPerceptionBoost = iPerceptionBoost;
	}

	public int getEnduranceBoost() {
		return this.iEnduranceBoost;
	}

	public void setEnduranceBoost(int iEnduranceBoost) {
		this.iEnduranceBoost = iEnduranceBoost;
	}

	public int getCharismaBoost() {
		return this.iCharismaBoost;
	}

	public void setCharismaBoost(int iCharismaBoost) {
		this.iCharismaBoost = iCharismaBoost;
	}

	public int getIntelligenceBoost() {
		return this.iIntelligenceBoost;
	}

	public void setIntelligenceBoost(int iIntelligenceBoost) {
		this.iIntelligenceBoost = iIntelligenceBoost;
	}

	public int getLuckBoost() {
		return this.iLuckBoost;
	}

	public void setLuckBoost(int iLuckBoost) {
		this.iLuckBoost = iLuckBoost;
	}

	public int getAgilityBoost() {
		return this.iAgilityBoost;
	}

	public void setAgilityBoost(int iAgilityBoost) {
		this.iAgilityBoost = iAgilityBoost;
	}
	
	public boolean getEquipped() {
		return this.bEquipped;
	}

	public void setEquipped(boolean bEquipped) {
		this.bEquipped = bEquipped;
	}


}

