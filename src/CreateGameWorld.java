import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * @(#)CreateGameWorld.java
 *  A class where the game's world is created. All the necessary data is parsed from a XML file that contains all rooms, exits and enemies. 
 *  This is a better alternative than hard-coding everything related to the game world. The original example put all needed data into a file in dat-format .
 *  TODO: Parsing Apparel objects and assigning effects on GameCharacter stats when equipped
 *  TODO: Parsing Exit objects and assigning them to a Room object 
 * 	@author Insill 
 **/
public class CreateGameWorld extends GameWorld{
//	public static String file = "maailma.dat";
	public enum effects {
		strength,
		perception,
		endurance,
		charisma,
		intelligence,
		agility,
		luck
	}
	public GameWorld createWorld(GameWorld game) {
		// gameWorld
	try {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse("src/gameworld.xml");
		NodeList rooms = document.getElementsByTagName("room"); 
		NodeList exits = document.getElementsByTagName("exit"); 
		NodeList enemies  = document.getElementsByTagName("enemies"); 
		NodeList items = document.getElementsByTagName("items"); 
		NodeList weapons = document.getElementsByTagName("weapons");
		NodeList apparels = document.getElementsByTagName("apparels"); 
		
		// Parsing rooms and exits. This is fucking clumsy code, I know

		for(int i = 0; i < rooms.getLength(); i++) {
			Element elementRoom = (Element) rooms.item(i);
			Room room = new Room();
			room.setTitle(elementRoom.getElementsByTagName("name").item(0).getTextContent().trim());
			room.setDescription(elementRoom.getElementsByTagName("description").item(0).getTextContent());
			room.setBattleLikehoodRatio(Integer.parseInt(elementRoom.getElementsByTagName("battlelikehoodratio").item(0).getTextContent()));
			game.addRoom(room);
 		}
		
		for(int i = 0; i < exits.getLength(); i++) {
			Element elementExit = (Element) exits.item(i);
			int iDirection = Arrays.asList(Exit.DirectionName).indexOf(elementExit.getElementsByTagName("direction").item(0).getTextContent());
			String sLeadsTo = elementExit.getElementsByTagName("leadsto").item(0).getTextContent().trim(); 
			String sName = elementExit.getElementsByTagName("name").item(0).getTextContent();
			Exit exit = new Exit(sName,iDirection,game.getRoom(sLeadsTo)); //I crreated a method to get a Room object by a title 
			game.addExit(exit);
			System.out.println(exit.getName() + " " + exit.getDirectionName());
		}
		// The parser seems to get only one exit at time, strange 
		for(Room room: game.getRooms()) {
			for(Exit exit: game.getExits()) {
				String sName = exit.getName(); 
				System.out.println(sName); 
				if(sName.equals(document.getElementsByTagName("roomexits").item(0).getTextContent().trim())) {
					room.addExit(exit);
				}
			}
		}
		game.setCurrentRoom(game.getRoom(document.getElementsByTagName("currentroom").item(0).getTextContent()));
	
		for(int i = 0; i < enemies.getLength(); i++) {
			Node node = enemies.item(i); 
			Enemy enemy = new Enemy();
			Element element = (Element) node; 
			enemy.setName(element.getElementsByTagName("name").item(0).getTextContent().trim());
			enemy.setState(element.getElementsByTagName("state").item(0).getTextContent());
			enemy.setDescription(element.getElementsByTagName("description").item(0).getTextContent());
			enemy.setLevel(Integer.parseInt(element.getElementsByTagName("level").item(0).getTextContent()));
			enemy.setMaxHitPoints(Integer.parseInt(element.getElementsByTagName("hitpoints").item(0).getTextContent()));
			enemy.setCurrentHitPoints(enemy.getMaxHitPoints());
			game.addEnemy(enemy);
		}
		for(int i = 0; i < items.getLength(); i++) {
			Node node = items.item(i);
			Item item = new Item();
			Element element = (Element) node; 
			item.setName(element.getElementsByTagName("name").item(0).getTextContent().trim());
			item.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
			item.setDescription(element.getElementsByTagName("description").item(0).getTextContent());
			item.setUsable(Boolean.getBoolean(element.getElementsByTagName("usable").item(0).getTextContent()));
			item.setAmount(Integer.parseInt(element.getElementsByTagName("amount").item(0).getTextContent()));
			game.addItem(item);
		}
		for(int i = 0; i < weapons.getLength(); i++) {
			Node node = weapons.item(i); 
			Weapon weapon = new Weapon(); 
			Element element = (Element) node; 
			weapon.setName(element.getElementsByTagName("name").item(0).getTextContent().trim());
			weapon.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
			weapon.setDescription(element.getElementsByTagName("description").item(0).getTextContent());
			weapon.setLevel(Integer.parseInt(element.getElementsByTagName("level").item(0).getTextContent()));
			weapon.setType(element.getElementsByTagName("type").item(0).getTextContent()); 
			weapon.setMinDamage(Integer.parseInt(element.getElementsByTagName("mindamage").item(0).getTextContent())); 
			weapon.setMaxDamage(Integer.parseInt(element.getElementsByTagName("maxdamage").item(0).getTextContent())); 
			game.addWeapon(weapon); 
		}
		for (int i = 0; i < apparels.getLength(); i++) {
			Node node = apparels.item(i); 
			Apparel apparel = new Apparel(); 
			Element element = (Element) node; 
			apparel.setName(element.getElementsByTagName("name").item(0).getTextContent().trim());
			apparel.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
			apparel.setDescription(element.getElementsByTagName("description").item(0).getTextContent());
			if(element.getElementsByTagName("effects") != null) {
				String effect = String.valueOf(element.getElementsByTagName("effects")); 
				switch(effects.valueOf(effect)) {
					default:
						break;
					case strength: apparel.setStrengthBoost(Integer.parseInt(element.getElementsByTagName("strength").item(0).getTextContent())); break; 
					case perception: apparel.setPerceptionBoost(Integer.parseInt(element.getElementsByTagName("perception").item(0).getTextContent())); break; 
					case endurance: apparel.setEnduranceBoost(Integer.parseInt(element.getElementsByTagName("endurance").item(0).getTextContent())); break; 
					case charisma: apparel.setCharismaBoost(Integer.parseInt(element.getElementsByTagName("charisma").item(0).getTextContent())); break; 
					case intelligence: apparel.setIntelligenceBoost(Integer.parseInt(element.getElementsByTagName("intelligence").item(0).getTextContent())); break; 
					case agility: apparel.setAgilityBoost(Integer.parseInt(element.getElementsByTagName("agility").item(0).getTextContent())); break; 
					case luck: apparel.setLuckBoost(Integer.parseInt(element.getElementsByTagName("luck").item(0).getTextContent())); break; 
				}
			}
			game.addApparel(apparel);
		}
	}
	catch(Exception e){
		e.printStackTrace();
	}

	return game;		
	}

}
