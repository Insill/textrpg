import java.util.*; 
import java.io.*;
/**
 * @(#)Textgame.java 
 * 
 * The main class of a text RPG game. Takes care of running the game loop, actions and exiting the game.
 * 
 * @author Insill
 * @version 1.00 2016/6/17
 **/
public class TextGame {

	private GameWorld game = new GameWorld();
	
	public TextGame() {
 
		try {
			CreateGameWorld createGameWorld = new CreateGameWorld(); 
			game = createGameWorld.createWorld(game);
			game.setOutputStream ( System.out, 40 );
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String args[]) throws Exception {
	
		System.out.println("----||---------||-------||--------||---------||-------||----\n");
		System.out.println("\n\tText RPG (version 1.0)\n");
		System.out.println("\n\tInsill \n");
		System.out.println("\n\t2013\n");
		System.out.println("----||---------||-------||--------||---------||-------||----\n");
		System.out.println("\nWelcome!");

		new TextGame().play();
	}	
	
	public void exit() {
		System.out.println("The game is over, bye bye! "); 
		System.exit(0);
	}
	
		public void play() {
	        GameCharacter player = CharacterCreation.createGameCharacter();
	        Random random = new Random(); 
			String command = " ";
			boolean commandAccepted;
			int battleLikehood = random.nextInt(255-64) + 64;
			// Create a data input stream
			Scanner scanner = new Scanner(System.in); 
			for (;;)	{
				// Show location
				game.showRoom();
				command = scanner.nextLine();
					
				// Print a new line
				System.out.println();
	
				// By default, we haven't found a valid command
				commandAccepted = false;
	
				// Parse user input
				if (command.length() == 0)	{
					System.out.println ("\nYou didn't give any commands!");
					continue;
				} else if(command.contains("Q")) {
					exit(); break; 
				}
				// Check to see if user wants to quit
				
	
				// Convert to uppercase for comparison
				command = command.toUpperCase();
				// TODO: replace search for an exit with handling a command; is the player going to a different room or is he exploring?
				// Search for an exit match
				Iterator<Exit> iterator = game.getCurrentRoom().getExits().iterator(); 
				while (iterator.hasNext()) {
					Exit exit = iterator.next();
	
					if ( (exit.getDirectionName().compareTo(command) == 0) || (exit.getDirectionShortName().compareTo(command) == 0 )) {
						// Set location to the location pointed to by exit
						game.setCurrentRoom( exit.getLeadsTo() );
						// The battleLikehood variable decrements every time the player moves into a different room, a room can have a different rate when the variable decrements
						battleLikehood = battleLikehood - game.getCurrentRoom().getBattleLikehoodRatio(); 
						// Valid command encountered
						System.out.println(battleLikehood); 
						commandAccepted = true;
						// No need to search exits anymore
						break;
					}				
				}
				// If the variable reaches zero, we get into a battle 
				// TODO: If a battle is lost, go to exit() method 
				if(battleLikehood < 0 || battleLikehood == 0) {
					Battle battle = new Battle();
					if(battle.victory() == true) 
						battleLikehood = random.nextInt(255-64) + 64; 
					else if(battle.death() == true)
						exit();
				}
				
				
	
				// If no valid commands, warn the user is invalid
		}
	}
}