
class Weapon extends Item {
	
	private String sType; 
	private boolean bEquipped = false; 
	private double dPower;
    private int iMinDamage;
    private int iMaxDamage;
	private int iLevel; 
	  
	public String sAllAttributes = ""; 
	

	public Weapon(String sName, String sDescription,int iPrice, int iAmount, boolean bUsable, int iLevel, double dPower, int iMinDamage, int iMaxDamage) {
	
		super(sName, sDescription, iPrice, iAmount);
        this.iLevel = iLevel; 
		this.dPower = dPower; 
        this.iMinDamage = iMinDamage;
		this.iMaxDamage = iMaxDamage; 
		this.sType = " "; 
	}
	
	public Weapon(String sName, String sDescription) {
		super(sName,sDescription); 
		this.iLevel = 1; 
		this.dPower = 1.0; 
		this.iMinDamage = 2; 
		this.iMaxDamage = 6; 
		this.sType = " "; 
	}
	public Weapon() {
		super(); 
	}
	@Override
	public String toString() {
		
		sAllAttributes +=  super.toString();
		sAllAttributes += "------------\nLevel: " + iLevel + "\n";
		sAllAttributes += "Power: " + dPower + "\n";
        sAllAttributes += "Type: "  + sType + "\n"; 
        sAllAttributes += "Damage (min): " + iMinDamage + "\n";
		sAllAttributes += "Damage (max): " + iMaxDamage + "\n------------"; 
		
		return sAllAttributes;
		
	}
    
    public int getLevel() {
        return iLevel;
    }
	public void setLevel(int iLevel) {
		this.iLevel = iLevel; 
	}
	public void setPower(double dPower) {
		this.dPower = dPower; 
	}
    public double getPower() {
        return dPower; 
    }

	public int getMinDamage() {
		return iMinDamage;
	}
	public int getMaxDamage() {
		return iMaxDamage; 
	}
	public void setMinDamage(int iMinDamage) {
		this.iMinDamage = iMinDamage; 
	}
	public void setMaxDamage(int iMaxDamage) {
		this.iMaxDamage = iMaxDamage;
	}

	public boolean getEquipped() {
		return bEquipped;
	}

	public void setEquipped(boolean bEquipped) {
		this.bEquipped = bEquipped;
	}
	public void setType(String sType) {
		this.sType = sType; 
	}
	public String getType(String sType) {
		return sType; 
	}
  }

