
/**
* A class for enemy characters. Extends the Character class.
* TODO: Functionality for each enemy's loot that is dropped in battle, possibly some battle functionality
* @author Insill
**/
public class Enemy extends Character {

	private int iLevel;
	public String sAllAttributes = ""; 
	 
	public Enemy(String sName, String sState, String sDescription) {
		super(sName, sState, sDescription);
	}
	public Enemy() {
		super();
	}
	public String toString() {
		sAllAttributes += super.toString();
		return sAllAttributes; 
		
	}
}

