import java.util.Random; 
import java.util.ArrayList; 
public class EnemyFactory extends GameWorld {
	Random random = new Random(); 
	
	public Enemy createEnemy() {
		ArrayList enemies = getEnemies();
		int randomInt = random.nextInt(enemies.size()); 
		Enemy enemy = (Enemy) enemies.get(randomInt); 
		return enemy; 
	}
}
