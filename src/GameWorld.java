import java.util.*;
import java.io.*;
/**
*
* GameWorld
* An object representing the game's world with rooms ands exits. All the possible enemies in the world are stored in an ArrayList names Enemies. 
* The code is based on a text adventure code example presented here: http://www.javacoffeebreak.com/text-adventure/index.html 
* It has been modified to suit my game project better. 
*
* 
* Original code 1998, 1999, 2000
* Modified by Insill
* @author David Reilly
* @author Insill
*/
@SuppressWarnings("serial")
public class GameWorld implements Serializable {
	
	private ArrayList<Item> Items; 
	private ArrayList<Apparel> Apparels; 
	private ArrayList<Weapon> Weapons; 
	private ArrayList<Enemy> Enemies; 
	// List of Location objects
	private ArrayList<Room> Rooms;
	// List of Exit objects
	private ArrayList<Exit> Exits;
	
	// The current location of the player
	private Room currentRoom;

	// Character width for descriptions
	private int charWidth;

	// Output stream for gaming system21
	transient private WidthLimitedOutputStream output;

	// GameWorld constructor
	public GameWorld()
	{
		// Instantiate ArrayList lists for location/exits
		Items = new ArrayList<Item>(); 
		Weapons = new ArrayList<Weapon>(); 
		Apparels = new ArrayList<Apparel>(); 
		Enemies = new ArrayList<Enemy>(); 
		Rooms = new ArrayList<Room>();
		Exits = new ArrayList<Exit>();

		// The default location of a player isn't known
		currentRoom = null;
		
		// By default, use standard output
		setOutputStream (System.out, 80);
	}

	// GameWorld constructor
	public GameWorld(int characterWidth )
	{
		// Call default constructor
		this();

		charWidth = characterWidth;		
	}

	/** Returns the current location of the player */
	public Room getCurrentRoom()
	{
		return currentRoom;
	}
	public Room getRoom(String title) {
		Room searchRoom = new Room(); 
		for(Room room: Rooms) {
			if(room.getTitle().contains(title)) {
				searchRoom = room; 
			}
		}
		return searchRoom;
	}
	/** Assigns a new location to the current location of the player */
	public void setCurrentRoom(Room newRoom)
	{
		currentRoom = newRoom;
	}
	public void addItem(Item item) {
		if(!Items.contains(item))  
			Items.add(item);
	}
	
	public void addEnemy(Enemy enemy) {
		if(! Enemies.contains(enemy))
			Enemies.add(enemy); 
	}
	public void addWeapon(Weapon weapon) {
		if(!Weapons.contains(weapon)) 
			Weapons.add(weapon);
	}
	public void addApparel(Apparel apparel) {
		if(!Apparels.contains(apparel))
			Apparels.add(apparel); 
	}
	public ArrayList<Enemy> getEnemies() {
		return(ArrayList<Enemy>) Enemies; 
	}
	public ArrayList<Item> getItems() {
		return(ArrayList<Item>) Items; 
	}
	public ArrayList<Apparel> getApparels() {
		return(ArrayList<Apparel>) Apparels; 
	}
	public ArrayList<Weapon> getWeapons() {
		return(ArrayList<Weapon>) Weapons; 
	}
	public ArrayList<Room> getRooms() {
		return(ArrayList<Room>) Rooms; 
	}
	public ArrayList<Exit> getExits() { 
		return(ArrayList<Exit>) Exits; 
	}
	/** Adds a new exit to the gaming system */
	public void addExit( Exit exit )
	{
		// Check if exit ArrayList already contains exit 
		if (! Exits.contains ( exit ) )
			// Exit doesn't exist, and must be added
			Exits.add ( exit);
	}

	/** Adds a new location to the gaming system */
	public void addRoom( Room room )
	{
		// Check if location ArrayList already contains location 
		if (! Rooms.contains ( room ) )
			// Location doesn't exist, and must be ad2ded
			Rooms.add ( room );
	}

	/** Sets the output stream for the gaming systewm */
	public void setOutputStream(OutputStream out, int width)
	{	
		output = new WidthLimitedOutputStream(out, width);
	}

	/** Shows the current game location */
	public void showRoom() {
		
		// Show title
		output.println ("------ " + currentRoom.getTitle()  + " ------");
		// Show description

		output.println ( currentRoom.getDescription() );
		output.println(); 

		// Show available exits		
		output.println ( "\nYou can go to:\n" );
		Iterator<Exit> iterator = currentRoom.getExits().iterator(); 
		// Traverse elements of ArrayList
		while (iterator.hasNext()) 	{
			// Get next exit
			Exit exit = iterator.next();

			// Print exit to our output stream
			output.println (exit.toString());
		}	
	}

}
