import java.util.ArrayList;
import java.util.Random;

/**
 * The factory class responsible for creating items when needed, for example when exploring or after winning a battle.
 * TODO: Logic for generating an item and getting the gameworld's list of items
 * @author Insill 
 **/
public class ItemFactory extends GameWorld{
	Random random = new Random(); 
	public GameWorld createRandom() {

		int choice = random.nextInt(4); 
		
		switch(choice)  {
			case 0: return null; 
			case 1: Item item = null; createItem(item); 
			case 2: Apparel apparel = null; createApparel(apparel); 
			case 3: Weapon weapon = null; createWeapon(weapon); 
		}
		return null;
	}
	public Item createItem(Item item) {
		ArrayList<Item> Items = getItems(); 
		int choice = random.nextInt(Items.size()); 
		item = (Item) Items.get(choice); 
		return item; 
	}
 
	public Apparel createApparel(Apparel apparel) { 
		ArrayList<Apparel> Apparels = getApparels(); 
		int choice = random.nextInt(Apparels.size()); 
		apparel = (Apparel) Apparels.get(choice); 
		return apparel; 
	}
	
	public Weapon createWeapon(Weapon weapon) {
		ArrayList<Weapon> Weapons = getWeapons(); 
		int choice = random.nextInt(Weapons.size()); 
		weapon = (Weapon) Weapons.get(choice); 
		return weapon; 
	}
	
 
}
