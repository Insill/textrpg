/**
 * An interface for Item objects. What happens when an item is used? 
 * @author Insill
 *
 */
public interface ItemInterface {
	public void use(); 
}
