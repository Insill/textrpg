
/**
 * A class representing the player's inventory. The inventory consists of two ArrayLists for items (that have to be used manually), weapons (can be equipped and are used in battle) and apparel 
 * (can enhance the player's attributes and provide defense). The class contains all necessary operations for adding, removing and searching items. 
 * @author Insill
 */

import java.util.ArrayList;

public class Inventory {

    ArrayList<Item> Items = new ArrayList<Item>(); 
    ArrayList<Weapon> Weapons = new ArrayList<Weapon>();
    ArrayList<Apparel> Apparels = new ArrayList<Apparel>(); 
    
    public Inventory() {
    	ArrayList<Item> Items = new ArrayList<Item>(); 
        ArrayList<Weapon> Weapons = new ArrayList<Weapon>();
        ArrayList<Apparel> Apparels = new ArrayList<Apparel>(); 
    }
    
    public ArrayList<Weapon> getWeapons() {
    	return(ArrayList<Weapon>) Weapons; 
    }
    
    public ArrayList<Item> getItems()  {
		return(ArrayList<Item>) Items;    		
    }
    
    public ArrayList<Apparel> getApparels() {
    	return(ArrayList<Apparel>) Apparels; 
    }
    
    public void printAll() {
        if(this.Items.size() == 0 && this.Weapons.size() == 0 && this.Apparels.size() == 0) {
            System.out.println("You have nothing!");
        }
        else {
            printItems();
			printWeapons();
			printApparels();
        }
    }
    
    public void printWeapons() {
       
        if(Weapons.size() > 0) {
        	System.out.println("\n------ Weapons -----\n");
	        for (Weapon weapon : Weapons) {
	            System.out.println(Weapons.indexOf(weapon) + " " + weapon.getName()); 
	        }
	        System.out.println("----------------------");
        } else {
        	System.out.println("You have no weapons in your inventory!");
        }
    }
    
    public void printItems() {
		
		if(Items.size() > 0) {
			System.out.println("\n------ Items -----\n");
	        for (Item apparel : Items) {
	            System.out.println(Items.indexOf(apparel) + " " + apparel.getName()); 
	        }
	        System.out.println("----------------------");
		} else {
			System.out.println("You have no apparels in your inventory!"); 
		}
    }
    
    public void printApparels() {
		
 		if(Apparels.size() > 0) {
 			System.out.println("\n------ Apparel -----\n");
 	        for (Apparel apparel : Apparels) {
 	            System.out.println(Items.indexOf(apparel) + " " + apparel.getName()); 
 	        }
 	        System.out.println("----------------------");
 		} else {
 			System.out.println("You have no apparel in your inventory!"); 
 		}
     }
    public void useItem(Item item) {
    	if(Items.contains(item) && item.getUsable() == true) {
    		item.use();	// After using, an item will be removed 
    		removeItem(item); 
    	}
    }
    
    public void addItem(Item apparel) {
            if (Items.contains(apparel)){
                apparel.setAmount(apparel.getAmount() + 1); 
                this.Items.add(apparel);
            }  
    }
    
    public Item getItem(Item apparel) {
    		if(Items.contains(apparel)) {
    			return apparel; 
    		}
    		else {
    			return null; 
    		}
    }
    
    public void removeItem(Item item) {
            if (Items.contains(item)) {
                item.setAmount(item.getAmount() - 1); 
                this.Items.remove(item);
                if(item.getAmount() == 0) {
                	item = null; 
                }
            }
    }
    public Weapon getWeapon(Weapon weapon) {
    		if(Weapons.contains(weapon)) {
    			return weapon; 
    		}
    		else {
    			return null; 
    		}
    }
    
    public void addWeapon(Weapon weapon) {
            if (Weapons.contains(weapon)){
                weapon.setAmount(weapon.getAmount() + 1); 
                this.Weapons.add(weapon);
            }
    }

    public void removeWeapon(Weapon weapon) {
            if (Weapons.contains(weapon)){
                weapon.setAmount(weapon.getAmount() - 1); 
                this.Weapons.remove(weapon);
            }   
    }
    
    
    public void addApparel(Apparel apparel) {
            if (Apparels.contains(apparel)){
                apparel.setAmount(apparel.getAmount() + 1); 
                this.Apparels.add(apparel);
            }         
    }
    
    public Apparel getApparel(Apparel apparel) {
    		if(Apparels.contains(apparel)) {
    			return apparel; 
    		}
    		else {
    			return null; 
    		}
    }
    
    public void removeApparel(Apparel apparel) {
            if (Apparels.contains(apparel)) {
                apparel.setAmount(apparel.getAmount() - 1); 
                this.Apparels.remove(apparel);
            }    
    }
    public boolean isEmpty() {
		if(this.Weapons.size() == 0 && this.Items.size() == 0 && this.Apparels.size() == 0) {
			return true; 
		} else {
			return false; 
		}
    }
}
	