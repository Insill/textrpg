import java.io.Serializable;
/*
* Exit - represents an exit to a location
* 
* Last modification date : October 07, 1997
**/
@SuppressWarnings("serial")
public class Exit implements Serializable
{
	// Numerical codes
	public static final int UNDEFINED = 0;
	public static final int NORTH = 1;
	public static final int SOUTH = 2;
	public static final int EAST = 3;
	public static final int WEST  = 4;
	public static final int UP   = 5;
	public static final int DOWN  = 6;
	public static final int NORTHEAST = 7;
	public static final int NORTHWEST = 8;
	public static final int SOUTHEAST = 9;
	public static final int SOUTHWEST = 10;
	public static final int IN = 11;
	public static final int OUT = 12;

	// String codes	
	public static final String[] DirectionName = 
	{ 
		"UNDEFINED",
		"NORTH",
		"SOUTH",
		"EAST",
		"WEST",
		"UP",
		"DOWN",
		"NORTHEAST",
		"NORTHWEST",
		"SOUTHEAST",
		"SOUTHWEST",
		"IN",
		"OUT",
	};

	public static final String[] DirectionShortName = 
	{
		"NULL",
		"N",
		"S",
		"E",
		"W",
		"U",
		"D",
		"NE",
		"NW",
		"SE",
		"SW",
		"I",
		"O",
	};

	// Member variables
	private Room leadsTo = null;
	private int iDirection;
	// Full name of direction eg SOUTHEAST
	private String sDirectionName;
    private String sName; 
	// Shortened version of direction eg SE
	private String sDirectionShortName;

	// Default constructor
	public Exit()
	{
		this.sName = "e1"; 
		this.iDirection = Exit.UNDEFINED;
		this.leadsTo = null;
		this.sDirectionName = DirectionName[UNDEFINED];
		this.sDirectionShortName = DirectionShortName[UNDEFINED];
	}

	// Full constructor
	public Exit(String sName, int direction, Room leadsToName )
	{
		this.iDirection = direction;
		this.sName = sName; 
		// Assign direction names
		if (direction <= DirectionName.length )
			this.sDirectionName = DirectionShortName[direction];
		if (direction <= DirectionShortName.length )
			this.sDirectionShortName = DirectionShortName[iDirection];

		// Assign location
		this.leadsTo = leadsToName;
	}
	public String getName() {
		return sName; 
	}
	// toString method
	public String toString()
	{
		return sDirectionName;
	}

	// Assigns direction name
	public void setDirectionName( String directionName )
	{
		sDirectionName = directionName;
	}

	// Returns direction name
	public String getDirectionName()
	{
		return sDirectionName;
	}

	// Assigns short direction name
	public void setDirectionShortName ( String directionShort )
	{
		sDirectionShortName = directionShort;
	}

	// Returns short direction name
	public String getDirectionShortName ()
	{
		return sDirectionShortName;
	}

	// Assigns location
	public void setLeadsTo ( Room leadsToName )
	{
		leadsTo = leadsToName;
	}

	// Returns location
	public Room getLeadsTo ( )
	{
		return leadsTo;
	}

}

