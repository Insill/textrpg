/**
 * @(#)CharacterCreation.java
 *  This class creates the player's character. The character gets all the necessary stats (SPECIAL), the starting weapon and starting apparel. This is now a basic class so the character is created by hard-coding.
 *  TODO: Let the user assign all necessary attributes, get apparel and weapon from an XML file instead of hard-coding
 *  @author Insill
 */
public class CharacterCreation {
	
	static GameCharacter createGameCharacter() {
		GameCharacter player = new GameCharacter("Insill", "normal", "Human", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		Weapon weapon = new Weapon("Rusty sword", "A short sword that is a little bit rusty. The hilt is decorated with mystical runes", 5, 1, true, 1, 1.0, 2, 8);
		Apparel apparel = new Apparel("Leather jacket", "A worn leather jacket that looks like it's been taken from the Matrix", 10,1);
		apparel.setEnduranceBoost(2);
		apparel.setCharismaBoost(1);
		Apparel apparel2 = new Apparel("Darksight goggles", "Special goggles for seeing details in dark", 30,1);
		apparel2.setPerceptionBoost(2);
		player.getInventory().addApparel(apparel);
		player.getInventory().addApparel(apparel2);
		player.getInventory().addWeapon(weapon);
		player.setEquippedApparel(apparel2);
		player.setEquippedApparel(apparel);
		player.setEquippedWeapon(weapon);
	
		return player; 
}

}