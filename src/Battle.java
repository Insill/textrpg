import java.util.Scanner;
/**
 * @(#)Battle.java
 * 
 * A class taking care of battle scenarios. Battles occur randomly and the player encounters an enemy. The player can attack, defend himself, use an item or run away.
 * I am planning to implement the battle mechanics of the first two Fallout games. 
 * @author Insill
 */
public class Battle extends GameWorld{
	

	static Scanner scanner = new Scanner(System.in); 
	static int command = 0; 
	static GameCharacter player; 
	static Enemy enemy; 
		
	public static void main(String[] args) {
		//TODO: Code for loading an enemy from list (some kind of random solution) 
		//TODO: Code for getting player attributes 
		EnemyFactory factory = new EnemyFactory(); 
		enemy = factory.createEnemy(); 
		System.out.println(enemy.getName() + " appears and wants to fight! What are you going to do?");
		battleLoop();
	}
	public static void battleLoop() {
		System.out.println("\n-------\n1 - Attack \n 2 - Defend\n3 - Use Item\n4 - Run\n--------\n");
		while(command == 0) {
			System.out.print("Give a command: ");
			command = scanner.nextInt();
		}
		switch(command) {
			case 1: attack(); 
			case 2: defend(); 
			case 3: useItem(); 
			case 4: run();
		}
	}
	//TODO: Logic for attacking and defending, logic for enemy's turn
	public static void attack() {
		System.out.println("You attack"); 
		battleLoop();
	}
	
	public static void defend() {
		System.out.println("You defend"); 
		battleLoop();
	}
	
	public static void useItem() {

		if(player.getInventory().getItems().size() > 0) {
			int itemChoice = -1; 
			System.out.println("What item do you want to use? "); 
			while(itemChoice < 0)  {
				itemChoice = scanner.nextInt(); 
			}
			if(player.getInventory().getItems().get(itemChoice) != null) {
				Item item; 
				item = player.getInventory().Items.get(itemChoice); 
				System.out.println("You use " + item.getName());
				player.getInventory().useItem(item);
			}
		} else {
			System.out.println("You have no items in your inventory!"); 
		}
		battleLoop();
	}
	// TODO: Handling winning a battle or dying, also handling possible loot dropped by the dead enemy and experience points. 
	public static void run() {
		System.out.println("You ran away!");
	}
	
	public boolean victory() {
		System.out.println("You defeat [Enemy] and win!");
		return true; 
	}
	
	public boolean death() {
		System.out.println("You lose a battle with [Enemy] and die!");
		return false; 
	}
}
