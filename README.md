# README #

This is my text-based RPG project made in Java. In terms of providing basic navigation functionality, some code is based on an example found here (made by David Reilly) http://www.javacoffeebreak.com/text-adventure/. The project has been expanded to include functionality required for RPGs, such as classes for the player and enemies, inventory management and battles. All necessary data needed for the game is included in an XML file that is parsed when the game starts in order to prevent hard coding. Originally all that data was stored in to a DAT file in a serializable form, this is going to be replaced by XML serialization. 
The game is going to implement partially the SPECIAL ruleset found in Fallout games, exluding mechanics that require moving in isometric space and don't fit into a text-based RPG. 

This project began development in 2013 and it shows by having horrible spaghetti code in some places. The development has been frozen since early 2018 because I felt like having faced a major roadblock. How to implement RPG and adventure functionality and parsing, what is the direction to be taken? Maybe some day I touch this project again....

# Classes #


## Character ##
The base class for characters that is extended by GameCharacter and Enemy classes. Every character has a name, state, description, hitpoints, inventory and equipped weapon and apparel. 

### GameCharacter ###
The class for player's character. GameCharacter has primary statistics (Strength, Perception, Endurance, Intelligence, Agility and Luck) which affect interaction with the game world and fighting. When there are enough experience points, GameCharacter levels up. Currently the creation of a GameCharacter is made by CharacterCreation class for testing purposes. This obviously changes in future. 

### Enemy ###
The class for enemies encountered during battles. Currently under construction. Enemies are spawned during random encounters with the EnemyFactory class.

## Item ##
The base class for items found in the game's world. Every item has a name, description, price, amount and a boolean telling whether it can be used. An Item object implements an ItemInterface which contains a method for using an item. This is because there can be items for various puropses: healing, poisoning an enemy and enhancing the player's stats for example. It is more reasonable to implement a method to decide what happens when an item is used rather than write subclasses for items used for various purposes.
This class is extended by Weapon and Apparel classes. A random Item is spawned with the ItemFactory class when necessary. 

### Weapon ###
A class for weapons used in battle. Every weapon has a range of damage it can deal during battle. Currently weapons have a level which can raise if enough experience is gained during battles. The damage range will thus increase until a maximum level is gained. This may be a feature that is implemented later, alongside with a weapon type.

### Apparel ###
A class for clothes and armors the player can equip. Equipping an Apparel can raise the player's attributes and protect from certain amount of damage, determined by certain variables such as (Attribute)Boost, armor class and damage threshold. This class is under construction.

## GameWorld ##
A GameWorld contains all rooms and exits required for navigation (see Room and Exit objects) and ArrayLists for items, weapons, apparels and enemies that can be found. A GameWorld object is created by reading an XML file when the game starts in class CreateGameWorld.

### CreateGameWorld ###
The CreateGameWorld class utilizes Java's built-in DOM XML parser. An XML file is parsed and all necessary objects are created by reading it and after that a GameWorld object is returned. Currently the class isn't very elegant in terms of fuctionality and code readability: helluva lot of for loops and the createWorld method is over 50 lines long which - according to my understanding - breaks certain code guidelines. Under construction, currently a Room gets one Exit object parsed and an Apparel object doesn't get parsed properly at all. 

## Room ##
A Room has a title, description, likehood of battle and an ArrayList containing Exit objects as attributes. The variable battleLikehoodRatio controls the likehood of a battle occurring when this room is visited (more about battleLikehoodRatio at TextGame). There are methods of adding Exit objects into the Exits ArrayList. During gameplay, when a Room is showed, the method getExits in order to get all the Exit objects attached to it and to show where the player can go next. 

## Exit ##
Exit object represents where you can go from this door. The iDirection variable determines a direction you can take and it is compared to arrays DirectionName and DirectionShortName. sName is only used for helping out parsing objects in CreateGameWorld class. LeadsTo represents the Room object where is possible to navigate. 

## Command ##

A class for processing all other commands than the ones related to navigation, such as accessing the inventory, showing the player's stats and searching the current room for possible loot. Under construction!

## Inventory ##

GameCharacter and Enemy both have an Inventory for all items, weapons and apparels. The Inventory has methods for accessing it which are used by other classes. There are three separate ArrayLists for items, weapons and apparels which are like pouches in a bag. 

## TextGame ##

TextGame handles running the game. Currently on start a GameWorld object is created by creating an instance of CreateGameWorld class and running it. After that, the player's stats are loaded and the first room is showed. The player gets directions for navigation (the room's exits are printed) and then he moves to the next room (the input is handled and interpreted). 
On start, the variable battleLikehood is initialized with a random value. When moving to another room, the variable is decremented depending on the rooms battlLikehoodRatio. When battleLikehood is zero or lower, we get into a battle with enemy (more in Battle). If the enemy gets killed by player, battleLikehood gets once again initialized with a random value, otherwise the game ends. 
TODO: implement loading and saving a game, replace creating a GameCharacter with parsing an XML file (or if there are no saves, with a dialogue in CharacterCreation) and battle related things.

## Battle ##
A class representing a battle situation. On start, a random enemy is created by using EnemyFactory. The player can choose between fighting, using an item or running away. There is going to be logic for implementing battling and turns. Under construction!